#ifndef LIDAR_H
#define LIDAR_H

#include <vector>
#include "observer.h"

class Lidar {
private:
    std::vector<Observer*> observers;

public:
    void subscribe(Observer* observer);
    void unsubscribe(Observer* observer);
    void notify(int obstacleStatus);
};

#endif // LIDAR_H