#ifndef CHASSIS_H
#define CHASSIS_H

#include "observer.h"

class Chassis : public Observer {
public:
    void update(int obstacleStatus) override;
};

#endif // CHASSIS_H