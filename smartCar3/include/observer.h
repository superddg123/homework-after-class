#ifndef OBSERVER_H
#define OBSERVER_H

class Observer {
public:
    virtual void update(int obstacleStatus) = 0;
};

#endif // OBSERVER_H
