#include <iostream>
#include "chassis.h"

void Chassis::update(int obstacleStatus) {
    if (obstacleStatus == 1) {
        std::cout << "后退" << std::endl;
    } else if (obstacleStatus == 2) {
        std::cout << "右转" << std::endl;
    } else if (obstacleStatus == 3) {
        std::cout << "左转" << std::endl;
    }
}

