#include "lidar.h"

void Lidar::subscribe(Observer* observer) {
    observers.push_back(observer);
}

void Lidar::unsubscribe(Observer* observer) {
    // 在这个示例中，我们不需要取消订阅功能
}

void Lidar::notify(int obstacleStatus) {
    for (auto observer : observers) {
        observer->update(obstacleStatus);
    }
}
