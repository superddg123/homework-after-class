#include <iostream>
#include "lidar.h"
#include "chassis.h"

using namespace std;

int main() {
    Lidar lidar;
    Chassis chassis;

    lidar.subscribe(&chassis);

    int obstacleStatus;
     cout << "请输入障碍物状态（1代表前方障碍，2代表左前方障碍，3代表右前方障碍）：" <<  endl;
     cin >> obstacleStatus;
    lidar.notify(obstacleStatus);

    return 0;
}
